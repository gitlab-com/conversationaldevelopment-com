---
redirect_from:
  - /why/
---
# Why Conversational Development?

Software development consists of conversations around what can be improved,
how to implement it, did it work and bring the expected value.

Experience has show us that these conversations can be summarized into four
principles to simplify the software development life cycle.

### [1. Shorten the conversation cycle](/shorten-cycle)

Start working on smaller pieces of functionality. Instead of minimum viable
products or minimum viable features, start thinking in terms of Minimum
Viable Changes to bring value to your customers and measure your cycle time as
soon as possible so you can iterate and improve.
[Read more about shortening the conversation cycle](/shorten-cycle).

### [2. Thread the conversation through all stages](/thread-conversations)

It is important to thread the conversation through the entire software
development process either by linking various tools or using a centralized
solution. A linked conversation with clearly defined stage transitions will
allow your team to measure the entire cycle time for the minimum viable change
as well as the time spent in each stage e.g design, development, testing etc.
[Read more about threading the conversation through all stages](/thread-conversations).

### [3. Open conversations without consensus](/open-conversations)

Instead of locking conversation on per-stage, per team, or per-specialty
principle, leave the doors as open as possible to increase the reuse of
solutions, prevent duplicate work, increase knowledge sharing and scale your
organization.
[Read more about opening the conversation](/open-conversations).

### [4. Result oriented conversation](/results-oriented)

Stop measuring abstract things like story points or irrelevant things like
hours. Instead pro-actively look for ways to connect your work to business
metrics and measure the time it takes to ship changes from an idea to production.
[Read more about creating a result orientated culture](/results-oriented).

