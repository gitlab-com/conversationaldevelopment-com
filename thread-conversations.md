---
---
## Principle 2: Thread the conversations through all stages

Instead of having multiple non-connected stage-related conversations have one 
going through all stages so that people who join later during the work could 
find the roots and restore the full picture.

<div>
  <div class='col-md-1'></div>
  <div class='col-md-5'>
    <img style="width:100%" src="/images/thread21.png" />
    <h4 class='center'>
      Before
    </h4>
  </div>
  <div class='col-md-1'></div>
  <div class='col-md-5'>
    <img style="width:100%" src="/images/thread22.png" />
    <h4 class='center'>
      After
    </h4>
  </div>
</div>

### Link everything

Make sure ideas are linked to issues, issues to MR's, MR's to deployments, 
deployments to monitoring.

Traceability is a best practice is many quality frameworks. Ideally, you 
should be able to go from a change in a graph to the idea in chat.

In the ideal world you'd be using the same tool for all the stages, so you'll 
get it for free.

In reality, you might have a zoo of tools, where each tool is used only at one 
stage. In this case, it would make sense to put additional efforts to make it 
possible.

The reason for that is that it is the only way to measure the cycle time (and 
therefore, shorten it) and have result oriented conversations.

[Principle #3: Open conversations without consensus](/open-conversations)
